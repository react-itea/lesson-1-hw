import React, { Component } from 'react'
import './App.css';
import { Header } from './components/Header'
import importGuests from './guests.json'
import { GuestItem } from './components/GuestItem'

const guests = importGuests.map(el => {
  return ({
    guest: el,
    arrived: false
  })
})

class App extends Component {

  state = {
    guests,
    filteredGuests: [],
    value: '',
    limit: 10
  }

  arrivedHandler = (id) => _ => {
    const newGuestsList = this.state.guests.map(el => {
      if (el.guest._id === id) {
        el.arrived = !el.arrived
      }
      return el
    })
    this.setState({
      guests: newGuestsList
    })
  }

  inputHandler = (e) => {
    const query = e.target.value.toLowerCase()
    const filteredGuests = this.state.guests.filter(el =>
      el.guest.name.toLocaleLowerCase().indexOf(query) !== -1 ||
      el.guest.company.toLocaleLowerCase().indexOf(query) !== -1 ||
      // el.guest.email.toLocaleLowerCase().indexOf(query) !== -1 ||
      el.guest.phone.toLocaleLowerCase().indexOf(query) !== -1 ||
      el.guest.address.toLocaleLowerCase().indexOf(query) !== -1
    )

    this.setState({
      value: query,
      filteredGuests
    })

  }

  limitFunc = (inpArr, limit) => inpArr.slice(0, limit)

  addHandler = () => {
    this.setState({
      limit: this.state.limit + 10
    })
  }

  showAll = (data) => _ => {
    this.setState({
      limit: data.length
    })
  }

  render = () => {

    const { guests, value, filteredGuests, limit } = this.state
    const { arrivedHandler, inputHandler, limitFunc, addHandler, showAll } = this

    let data = limitFunc(guests, limit)
    // let data = guests.slice(0, 10)
    if (filteredGuests.length > 0) { data = limitFunc(filteredGuests, limit) }

    return (
      <div className="container">
        <Header amount={filteredGuests.length > 0 || value.length > 0 ? filteredGuests.length : guests.length} />
        <input
          className="form-control"
          value={value}
          onChange={inputHandler}
        />
        <small id="emailHelp" className="form-text text-muted">Введите имя гостя для поиска.</small>
        {filteredGuests.length === 0 && value.length > 0
          ? <div className="alert alert-danger" role="alert">
            Не найдено!!!
            </div>
          : <table className="table table-hover">
              <tbody>
                {data.map(el => {
                  return (
                    <GuestItem
                      arrivedHandler={arrivedHandler}
                      guestEl={el}
                      key={el.guest._id}
                    />
                  )
                })}
              </tbody>
            </table>
        }
        <button
          type="button"
          className="btn btn-secondary btn-lg btn-block"
          onClick={addHandler}
        >
          Добавить 10
        </button>
        <button
          type="button"
          className="btn btn-secondary btn-lg btn-block"
          onClick={showAll(guests)}
        >
          Показать все
        </button>

      </div>
    )
  }
}

export default App;
