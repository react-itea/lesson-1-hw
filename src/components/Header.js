import React from 'react'

export const Header = (props) => {

  return(
    <h1>Example heading <span className="badge badge-secondary">{props.amount}</span></h1>
    // <h1>...</h1>
  )
}