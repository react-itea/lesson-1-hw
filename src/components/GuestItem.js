import React, { Component } from 'react'

export class GuestItem extends Component {


  render = () => {
    const { guestEl, arrivedHandler } = this.props
    const { guest, arrived } = guestEl

    return (
      <tr className={arrived ? 'guest-arrived' : ''}>
        <td>
          Гость <b>{guest.name}</b> работает в компании <b>{guest.company}</b>. <br />
          Его контакты: <br />
          <b>{guest.phone}</b> <br />
          <b>{guest.address}</b>
        </td>
        <td>
          <button
            type="button"
            className="btn btn-success"
            onClick={arrivedHandler(guest._id)}
          >
            {arrived ? 'Убыл' : 'Прибыл'}
          </button>
        </td>
      </tr>
    )
  }
}